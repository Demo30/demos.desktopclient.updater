﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demos.DemosHelpers;

namespace Demos.DesktopClient.Updater
{
    public class InfoData : IInfoData
    {
        public bool IsValid
        {
            get
            {
                if (!String.IsNullOrEmpty(UpdateDataURL) && !String.IsNullOrEmpty(ApplicationName) && Version != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string UpdateDataURL { get; set; }
        public string ApplicationName { get; set; }
        public Versioning Version { get; set; }
        public string[] ChangesInVersion { get { return this._changesInVersion.ToArray(); } }

        private List<string> _changesInVersion = new List<string>();

        public void AddChangeMessage(string[] messages)
        {
            if (messages == null || messages.Length == 0)
            {
                throw new ArgumentNullException();
            }

            foreach(string message in messages)
            {
                if (String.IsNullOrEmpty(message))
                {
                    throw new ArgumentNullException("None of the messages can be null.");
                }
            }

            this._changesInVersion.AddRange(messages);
        }

        public void AddChangeMessage(string message)
        {
            this.AddChangeMessage(new string[] { message });
        }
    }
}
