﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Security.Cryptography;

using System.IO;
using System.IO.Compression;
using System.IO.Packaging;
using System.Xml;
using System.Net;
using Demos.DemosHelpers;

namespace Demos.DesktopClient.Updater
{
    public class UpdaterManager : INewerVersionChecker
    {
        public const string SIGNED_PACKAGE_SIGNATURE_PART = "SIGNATURE";
        public const string SIGNED_PACKAGE_DATA_PART = "SIGNED_DATA";

        public const string UPDATEINFO_NODES_ROOT = "UpdateInfo";
        public const string UPDATEINFO_NODES_VERSION = "Version";
        public const string UPDATEINFO_NODES_DATAURL = "UpdateDataPackageURL";
        public const string UPDATEINFO_NODES_APPNAME = "ApplicationName";

        private enum DirOrFile { Directory, File }
        private const string AlgorithmName = "SHA1";
        private string DefaultConfigurationFileLocation { get { return Environment.CurrentDirectory; } }

        private InfoData _lastLoadedInfoDataPackage = null;

        public bool ValidDataPackageLoaded
        {
            get { return this._loadedUpdateData == null ? false : true; }
        }

        public event EventHandler<EventArgs> AvailableDataFileChanged;
        public UpdaterConfiguration Config { get; private set; }
        private byte[] LoadedUpdateData
        {
            get { return this._loadedUpdateData; }
            set
            {
                this._loadedUpdateData = value;
                try { if (this.AvailableDataFileChanged != null) { this.AvailableDataFileChanged(this, EventArgs.Empty); } } catch { }
            }
        }

        private byte[] _loadedUpdateData = null;

        public UpdaterManager() { }

        public UpdaterManager(UpdaterConfiguration config) : base()
        {
            if (config == null)
            {
                throw new ArgumentNullException();
            }

            this.Config = config;
        }

        public bool LoadConfiguration(string directoryContainingConfigurationFile = "")
        {
            try
            {
                UpdaterConfiguration config = new UpdaterConfiguration();

                directoryContainingConfigurationFile = String.IsNullOrEmpty(directoryContainingConfigurationFile) ? this.DefaultConfigurationFileLocation : directoryContainingConfigurationFile;

                config.LoadConfiguration(directoryContainingConfigurationFile);

                this.Config = config;

                return true;
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// Creates signed data package. Private key is supplied from XML file during runtime.
        /// </summary>
        /// <param name="destinationFullFilename">Full path of the outputted package file.</param>
        /// <param name="data">Arbitrary data to be packaged and signed.</param>
        /// <param name="pathToKeys">Keys stored in XML format as outputted by RSACryptoServiceProvider</param>
        public void CreateDataPackage(string destinationFullFilename, byte[] data, string pathToKeys, byte[] updateInfo = null)
        {
            RSACryptoServiceProvider rsa = this.GetCryptoProviderFromStoredKeys(pathToKeys);

            byte[] signedHashValue = this.SignData(UpdaterManager.AlgorithmName, data, rsa);

            Dictionary<string, byte[]> chunksToSave = new Dictionary<string, byte[]>();
            chunksToSave.Add(UpdaterManager.SIGNED_PACKAGE_SIGNATURE_PART, signedHashValue);
            chunksToSave.Add(UpdaterManager.SIGNED_PACKAGE_DATA_PART, data);

            if (updateInfo != null && updateInfo.Length > 0)
            {
                chunksToSave.Add("UPDATE_INFO", updateInfo);
            }

            PackagingTool.PackageByteChunks(destinationFullFilename, chunksToSave);
        }

        public byte[] CreateUpdateInfoPackage(string destinationFullFilename, string pathToKeys, InfoData info)
        {
            if (String.IsNullOrEmpty(destinationFullFilename) || String.IsNullOrEmpty(pathToKeys))
            {
                throw new ArgumentException("Both destination path and path to keys must be specified.");
            }

            if (info == null || info.IsValid == false)
            {
                throw new ArgumentException("Info data not in valid state.");
            }

            // Creating UpdateInfo data package

            XmlDocument doc = new XmlDocument();

            XmlElement root = doc.CreateElement(UpdaterManager.UPDATEINFO_NODES_ROOT);
            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");
            XmlElement versionNode = doc.CreateElement(UpdaterManager.UPDATEINFO_NODES_VERSION);
            XmlElement dataURLNode = doc.CreateElement(UpdaterManager.UPDATEINFO_NODES_DATAURL);
            XmlElement appNameNode = doc.CreateElement(UpdaterManager.UPDATEINFO_NODES_APPNAME);

            versionNode.InnerText = info.Version.ToString();
            dataURLNode.InnerText = info.UpdateDataURL;
            appNameNode.InnerText = info.ApplicationName;

            doc.AppendChild(decl);
            doc.AppendChild(root);
            root.AppendChild(appNameNode);
            root.AppendChild(versionNode);
            root.AppendChild(dataURLNode);

            string xml = GeneralHelperClass.GetXMLInPlaintext(doc);
            byte[] xmlBinary = Encoding.UTF8.GetBytes(xml);

            // Signing the UpdateInfo data package
            RSACryptoServiceProvider rsa = this.GetCryptoProviderFromStoredKeys(pathToKeys);
            byte[] signature = this.SignData(UpdaterManager.AlgorithmName, xmlBinary, rsa);

            // Saving the UpdateInfo package
            Dictionary<string, byte[]> chunksToSave = new Dictionary<string, byte[]>();
            chunksToSave.Add(UpdaterManager.SIGNED_PACKAGE_SIGNATURE_PART, signature);
            chunksToSave.Add(UpdaterManager.SIGNED_PACKAGE_DATA_PART, xmlBinary);

            PackagingTool.PackageByteChunks(destinationFullFilename, chunksToSave);

            byte[] packageData = null;

            using (FileStream fs = new FileStream(destinationFullFilename, FileMode.Open))
            {
                packageData = new byte[fs.Length];
                fs.Read(packageData, 0, packageData.Length);
            }

            return packageData;
        }

        public void LoadUpdateData(string pathToDataFile, UpdaterConfiguration config)
        {
            bool fail = false;
            try
            {
                byte[] signature = null;
                byte[] data = null;

                using (FileStream fs = new FileStream(pathToDataFile, FileMode.Open))
                {
                    Package package = PackagingTool.LoadPackage(pathToDataFile, fs);
                    this.UnpackSignedUpdateDataPackage(package, out signature, out data);
                }

                bool validPackage = this.CheckDataAuthenticity(data, signature, config);

                if (validPackage)
                {
                    this.LoadedUpdateData = data;
                }
                else
                {
                    fail = true;
                }
            }
            catch
            {
                fail = true;
            }
            finally
            {
                if (fail)
                {
                    this.LoadedUpdateData = null;
                }
            }
        }

        public void LoadUpdateDataURL(string urlAddress, UpdaterConfiguration config)
        {
            bool fail = false;

            try
            {
                byte[] signature = null;
                byte[] data = null;

                using (WebClient wClient = new WebClient())
                {
                    byte[] packageData = wClient.DownloadData(urlAddress);

                    using (MemoryStream ms = new MemoryStream(packageData))
                    {
                        Package package = Package.Open(ms);
                        this.UnpackSignedUpdateDataPackage(package, out signature, out data);
                    }
                }

                bool validPackage = this.CheckDataAuthenticity(data, signature, config);

                if (validPackage)
                {
                    this.LoadedUpdateData = data;
                }
                else
                {
                    fail = true;
                }
            }
            catch
            {
                fail = true;
            }
            finally
            {
                if (fail)
                {
                    this.LoadedUpdateData = null;
                }
            }
        }

        public bool IsNewerVersionAvailable(Versioning currentVersion)
        {
            return this.IsNewerVersionAvailable(currentVersion, null);
        }

        public bool IsNewerVersionAvailable(Versioning currentVersion, UpdaterConfiguration config = null)
        {
            if (currentVersion == null)
            {
                throw new ArgumentNullException();
            }

            if (config == null)
            {
                config = this.Config;
            }

            if (config == null)
            {
                try
                {
                    if (this.LoadConfiguration())
                    {
                        config = this.Config;
                    }
                }
                catch { }
            }

            if (config == null)
            {
                throw new InvalidOperationException("No configuration instance available.");
            }

            InfoData infoData = null;

            try
            {
                infoData = this.RetrieveUpdateInfoPackage(config);
                this._lastLoadedInfoDataPackage = infoData;
            }
            catch
            {
                throw new Exception("Failed to load info data package.");
            }
            
            if (currentVersion < infoData.Version)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Performs web request to specified address, gets updateInfoPackage and reads the final update package URL.
        /// </summary>
        /// <param name="updateVersionURL"></param>
        /// <returns></returns>
        public InfoData RetrieveUpdateInfoPackage(UpdaterConfiguration config = null)
        {
            if (config == null)
            {
                config = this.Config;
            }

            if (config == null)
            {
                throw new InvalidOperationException("No configuration instance available.");
            }

            byte[] signature = null;
            byte[] data = null;

            using (WebClient wClient = new WebClient())
            {
                ServicePointManager.SecurityProtocol = ServicePointManager.SecurityProtocol | SecurityProtocolType.Tls12;
                byte[] packageData = wClient.DownloadData(config.CheckVersionURL);

                using (MemoryStream ms = new MemoryStream(packageData))
                {
                    Package package = Package.Open(ms);
                    this.UnpackSignedUpdateDataPackage(package, out signature, out data);
                }
            }

            bool validPackage = this.CheckDataAuthenticity(data, signature, config);

            if (validPackage)
            {
                InfoData info = new InfoData();

                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(Encoding.UTF8.GetString(data));

                    XmlNode urlNode = doc.GetElementsByTagName(UpdaterManager.UPDATEINFO_NODES_DATAURL)[0];
                    string url = urlNode.InnerText;

                    XmlNode appNameNode = doc.GetElementsByTagName(UpdaterManager.UPDATEINFO_NODES_APPNAME)[0];
                    string appName = appNameNode.InnerText;

                    XmlNode versionNode = doc.GetElementsByTagName(UpdaterManager.UPDATEINFO_NODES_VERSION)[0];
                    string versionString = versionNode.InnerText;


                    Versioning version = new Versioning();
                    version.LoadVersion(versionString);

                    info.ApplicationName = appName;
                    info.UpdateDataURL = url;
                    info.Version = version;

                    if (info.IsValid)
                    {
                        return info;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    throw new InvalidDataException($"Unexpected structure of recieved data. Could not process the package into a valid {nameof(InfoData)} object.");
                }
            }
            else
            {
                throw new InvalidDataException("Data authenticity verification failed.");
            }
        }

        public void PerformUpdate()
        {
            // checking valid state to perform update from
            if (this.ValidDataPackageLoaded == false || this.Config == null)
            {
                throw new InvalidOperationException("Invalid state. Valid update data have to be loaded and configuration has to be in valid state.");
            }

            this.DeleteContentOfRoot();

            this.ProcessContentOfDataPackage(this.LoadedUpdateData);
        }

        private void ProcessContentOfDataPackage(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            using (var zip = new ZipArchive(ms, ZipArchiveMode.Read))
            {
                foreach (ZipArchiveEntry entry in zip.Entries)
                {
                    using (Stream stream = entry.Open())
                    {
                        string curFullPath = String.Concat(this.Config.RootDirectoryPath, "\\", entry.FullName);

                        if (this.DirOrFileByPathInZipArchive(entry.FullName) == DirOrFile.Directory)
                        {
                            if (!Directory.Exists(curFullPath))
                            {
                                Directory.CreateDirectory(curFullPath);
                            }
                        }
                        else
                        {
                            if (File.Exists(curFullPath))
                            {
                                File.Delete(curFullPath);
                            }

                            using (FileStream fs = new FileStream(curFullPath, FileMode.CreateNew))
                            {
                                stream.CopyTo(fs);
                                fs.Flush();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Deletes all content of Root folder except for those that are specified to be ignored in the configuration
        /// </summary>
        private void DeleteContentOfRoot()
        {
            string[] dirsInRoot = Directory.GetDirectories(this.Config.RootDirectoryPath);
            for (int i = 0; i < dirsInRoot.Length; i++)
            {
                string curPath = dirsInRoot[i];
                if (Directory.Exists(curPath) == false)
                {
                    throw new Exception($"Could not find any directory on path: {curPath}");
                }

                if (!IsAmongIgnoredPaths(curPath))
                {
                    Directory.Delete(curPath, true);
                }

            }

            string[] filesInRoot = Directory.GetFiles(this.Config.RootDirectoryPath);
            for (int i = 0; i < filesInRoot.Length; i++)
            {
                string curPath = filesInRoot[i];
                if (File.Exists(curPath) == false)
                {
                    throw new Exception($"Could not find any file on path: {curPath}");
                }

                if (!IsAmongIgnoredPaths(curPath))
                {
                    File.Delete(curPath);
                }

            }
        }

        private RSACryptoServiceProvider GetCryptoProviderFromStoredKeys(string pathToKeys)
        {
            // Loads key pair XML data that were previously created by RSAPKCS1SignatureFormatter
            XmlDocument doc = new XmlDocument();
            doc.Load(pathToKeys);
            string keyPairXML = doc.InnerXml;

            // Uses the XML data to create RSACryptoServiceProvider instance
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(keyPairXML);

            return rsa;
        }

        private byte[] SignData(string hashAlgorithmName, byte[] data, RSACryptoServiceProvider formatter)
        {
            byte[] hashedData = this.UniformDataHashMethod(data);

            // RSAPKCS1SignatureFormatter encrypts (= signs) the hashed message/data resulting in a signed hash
            RSAPKCS1SignatureFormatter rsaFormatter = new RSAPKCS1SignatureFormatter(formatter);
            rsaFormatter.SetHashAlgorithm(hashAlgorithmName);
            byte[] signedHashValue = rsaFormatter.CreateSignature(hashedData);
            return signedHashValue;
        }

        private bool CheckDataAuthenticity(byte[] data, byte[] signature, UpdaterConfiguration config)
        {
            RSAParameters rsaKeyInfo = this.GetPublicKeyInfo(config);

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(rsaKeyInfo);
            RSAPKCS1SignatureDeformatter rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
            rsaDeformatter.SetHashAlgorithm(UpdaterManager.AlgorithmName);

            byte[] hashedData = this.UniformDataHashMethod(data);

            bool verdict = rsaDeformatter.VerifySignature(hashedData, signature);
            return verdict;
        }




        #region Supportive methods

        private RSAParameters GetPublicKeyInfo(UpdaterConfiguration config)
        {
            RSAParameters rsaKeyInfo = new RSAParameters();

            rsaKeyInfo.Modulus = config.PublicKey_ModulusBinary;
            rsaKeyInfo.Exponent = config.PublicKey_ExponentBinary;

            return rsaKeyInfo;
        }

        private void UnpackSignedUpdateDataPackage(Package package, out byte[] signature, out byte[] data)
        {
            PackagePart signaturePart = null;
            PackagePart dataPart = null;

            foreach (PackagePart part in package.GetParts())
            {
                if (part.Uri.ToString() == String.Concat("/", UpdaterManager.SIGNED_PACKAGE_SIGNATURE_PART))
                {
                    signaturePart = part;
                }
                else if (part.Uri.ToString() == String.Concat("/", UpdaterManager.SIGNED_PACKAGE_DATA_PART))
                {
                    dataPart = part;
                }
            }

            if (signaturePart == null || dataPart == null)
            {
                throw new InvalidOperationException("Either signature or data part was not found in the package.");
            }

            using (Stream str = signaturePart.GetStream())
            using (BinaryReader br = new BinaryReader(str))
            {
                signature = new byte[str.Length];
                br.Read(signature, 0, (int)str.Length);
            }

            using (Stream str = dataPart.GetStream())
            using (BinaryReader br = new BinaryReader(str))
            {
                data = new byte[str.Length];
                br.Read(data, 0, (int)str.Length);
            }
        }

        /// <summary>
        /// Hashes the data. Same implementation for both creation and loading.
        /// </summary>
        /// <param name="data"></param>
        private byte[] UniformDataHashMethod(byte[] data)
        {
            SHA1 hasher = SHA1.Create();
            byte[] hashedData = hasher.ComputeHash(data);
            return hashedData;
        }

        private bool IsAmongIgnoredPaths(string path)
        {
            if (this.Config.NamesToIgnore == null || this.Config.NamesToIgnore.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < this.Config.NamesToIgnore.Length; i++)
            {
                string curNameToIgnore = this.Config.NamesToIgnore[i];

                if (path.Contains(curNameToIgnore))
                {
                    return true;
                }
            }

            return false;
        }

        private DirOrFile DirOrFileByPathInZipArchive(string path)
        {
            if (path.Last() == '/')
            {
                return DirOrFile.Directory;
            }
            else
            {
                return DirOrFile.File;
            }
        }

        public bool DoesFileContainValidPrivateKey(string pathToFile, UpdaterConfiguration config)
        {
            try
            {
                byte[] randomData = new byte[] { 0xff, 0x0c, 0x00, 0x00, 0xf0, 0x0f, 0x01, 0x55, 0xd4, 0xff };

                RSACryptoServiceProvider rsa = this.GetCryptoProviderFromStoredKeys(pathToFile);

                byte[] signedHash = this.SignData(UpdaterManager.AlgorithmName, randomData, rsa);

                RSACryptoServiceProvider rsaPK = new RSACryptoServiceProvider();
                rsaPK.ImportParameters(this.GetPublicKeyInfo(config));

                RSAPKCS1SignatureDeformatter rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
                rsaDeformatter.SetHashAlgorithm(UpdaterManager.AlgorithmName);
                return rsaDeformatter.VerifySignature(this.UniformDataHashMethod(randomData), signedHash);
            }
            catch
            {
                return false;
            }
        }

        public bool IsFileAZipArchiveFile(string pathToFile)
        {
            if (File.Exists(pathToFile) == false)
            {
                return false;
            }

            byte[] data = null;

            try
            {
                using (FileStream fs = new FileStream(pathToFile, FileMode.Open))
                {
                    data = new byte[fs.Length];
                    fs.Read(data, 0, data.Length);
                }

                using (MemoryStream ms = new MemoryStream(data))
                using (var zip = new ZipArchive(ms, ZipArchiveMode.Read))
                {
                    if (zip.Entries.Count > 0)
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        public IInfoData GetLatestVersionInfoData()
        {
            if (this._lastLoadedInfoDataPackage != null)
            {
                return this._lastLoadedInfoDataPackage;
            }
            else
            {
                try
                {
                    UpdaterConfiguration config = null;
                    config = this.Config;
                    if (config == null)
                    {
                        this.LoadConfiguration();
                        config = this.Config;
                    }

                    if (config == null)
                    {
                        throw new InvalidOperationException("No configuration instance available.");
                    }

                    InfoData idp = this.RetrieveUpdateInfoPackage(config);

                    return idp;
                }
                catch
                {
                    if (this.Config == null)
                    {
                        throw new Exception($"Failed to retrieve update info package. Failed to load configuration from path: {this.DefaultConfigurationFileLocation}");
                    }
                    else
                    {
                        throw new Exception("Failed to retrieve update info package.");
                    }
                }
            }
            
        }

        #endregion
    }
}
