﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace Demos.DesktopClient.Updater
{
    public class UpdaterConfiguration
    {
        public const string CONFIG_FILENAME = "UpdaterConfiguration.xml";

        public const string CONFIG_ROOT_NODE = "WardenUpdater";
        public const string CONFIG_CHECKVERSIONURL_NODE = "CheckVersionURL";
        public const string CONFIG_IGNORE_ROOT_NODE = "FilesToIgnore";
        public const string CONFIG_IGNORE_ROW_NODE = "Ignore";
        public const string CONFIG_PUBLICKEY_ROOT_NODE = "PublicKey";
        public const string CONFIG_PUBLICKEY_MODULUS_NODE = "Modulus";
        public const string CONFIG_PUBLICKEY_EXPONENT_NODE = "Exponent";
        public const string CONFIG_ROOTUPDATEPATH = "ApplicationRootPath";
        public const string CONFIG_APPLICATIONNAME_NODE = "ApplicationName";


        public string RootDirectoryPath { get; set; }
        public string CheckVersionURL { get; set; }
        public string[] NamesToIgnore { get; set; }
        public string ApplicationName { get; set; }
        public byte[] PublicKey_ModulusBinary { get; set; }
        public byte[] PublicKey_ExponentBinary { get; set; }


        public void LoadConfiguration(string configurationFileDirectory)
        {
            string fullPath = String.Concat(configurationFileDirectory, "\\", UpdaterConfiguration.CONFIG_FILENAME);

            if (File.Exists(fullPath) == false)
            {
                throw new InvalidOperationException();
            }

            XmlDocument doc = new XmlDocument();
            doc.Load(fullPath);

            this.LoadAppRootPath(doc);
            this.LoadVersionCheckURL(doc);
            this.LoadFilenamesToIgnore(doc);
            this.LoadPublicKey(doc);
            this.LoadApplicationName(doc);
        }

        private void LoadAppRootPath(XmlDocument doc)
        {
            XmlNode node = doc.GetElementsByTagName(UpdaterConfiguration.CONFIG_ROOTUPDATEPATH)[0];
            string rootPath = node.InnerText;
            this.RootDirectoryPath = rootPath;
        }

        private void LoadVersionCheckURL(XmlDocument doc)
        {
            XmlNode node = doc.GetElementsByTagName(UpdaterConfiguration.CONFIG_CHECKVERSIONURL_NODE)[0];
            string checkVersionURL = node.InnerText;
            this.CheckVersionURL = checkVersionURL;
        }

        private void LoadFilenamesToIgnore(XmlDocument doc)
        {
            XmlNode ignoreRoot = doc.GetElementsByTagName(UpdaterConfiguration.CONFIG_IGNORE_ROOT_NODE)[0];

            List<string> filenamesToIgnore = new List<string>();

            foreach(XmlNode ignoreNode in ignoreRoot.ChildNodes)
            {
                if (ignoreNode.Name == UpdaterConfiguration.CONFIG_IGNORE_ROW_NODE)
                {
                    filenamesToIgnore.Add(ignoreNode.InnerText);
                }
            }

            this.NamesToIgnore = filenamesToIgnore.ToArray();
        }

        private void LoadPublicKey(XmlDocument doc)
        {
            XmlNode modulusNode = doc.GetElementsByTagName(UpdaterConfiguration.CONFIG_PUBLICKEY_MODULUS_NODE)[0];
            XmlNode exponentNode = doc.GetElementsByTagName(UpdaterConfiguration.CONFIG_PUBLICKEY_EXPONENT_NODE)[0];

            byte[] modulus = this.ProcessStringOfHexValues(modulusNode.InnerText);
            byte[] exponent = this.ProcessStringOfHexValues(exponentNode.InnerText);

            this.PublicKey_ModulusBinary = modulus;
            this.PublicKey_ExponentBinary = exponent;
        }

        private void LoadApplicationName(XmlDocument doc)
        {
            XmlNode node = doc.GetElementsByTagName(UpdaterConfiguration.CONFIG_APPLICATIONNAME_NODE)[0];
            string applicationName = node.InnerText;
            this.ApplicationName = applicationName;
        }

        private byte[] ProcessStringOfHexValues(string hexString)
        {
            List<byte> bytes = new List<byte>();

            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException();
            }

            for(int i = 0; i < hexString.Length; i += 2)
            {
                string cur = hexString.Substring(i, 2);
                byte curByte = Convert.ToByte(cur, 16);
                bytes.Add(curByte);
            }

            return bytes.ToArray();
        }









        //internal static byte[] PublicKey_ModulusBinary { get { return new byte[] { 0xc6, 0x9c, 0x73, 0x33, 0x03, 0x20, 0xcf, 0x57, 0x6d, 0x2f, 0xe6, 0xd7, 0x2a, 0xc0, 0xa0, 0x90, 0xd8, 0xf0, 0xff, 0x59, 0x60, 0x7c, 0x12, 0xcf, 0xf5, 0x62, 0x8b, 0xd1, 0x2b, 0xe7, 0x36, 0xf4, 0xc7, 0xfb, 0xab, 0xf5, 0x6a, 0x44, 0xad, 0x6a, 0x4b, 0xf7, 0xa9, 0x69, 0x42, 0xce, 0xa9, 0x2e, 0x4e, 0xe2, 0xca, 0x1b, 0x4a, 0x1d, 0x39, 0x7b, 0xfc, 0x27, 0x72, 0x5d, 0x10, 0xc0, 0xbb, 0xc3, 0x8a, 0xdc, 0x35, 0x59, 0x65, 0x4c, 0x2c, 0xee, 0x85, 0xba, 0x35, 0x27, 0xd9, 0x18, 0xc8, 0x4a, 0xe0, 0x24, 0x61, 0xff, 0x32, 0x1f, 0x05, 0x66, 0x7f, 0xcd, 0xae, 0xd7, 0xaa, 0x40, 0x9c, 0xd1, 0xe4, 0xda, 0x27, 0xc6, 0x26, 0xd5, 0x50, 0x99, 0x96, 0x5a, 0x9e, 0xe0, 0x98, 0xd8, 0x4a, 0x22, 0xab, 0x90, 0x4f, 0x6a, 0x77, 0xb1, 0x07, 0x61, 0x45, 0x9e, 0x34, 0xbc, 0x28, 0x39, 0xd9, 0x15 }; } }
        //internal static byte[] PublicKey_ExponentBinary { get { return new byte[] { 0x01, 0x00, 0x01 }; } }

        //internal static byte[] PublicKey_ModulusBinary { get { return new byte[] { 0xb1, 0x9f, 0x3a, 0x3d, 0x37, 0x60, 0xc1, 0x75, 0x20, 0x66, 0x11, 0x23, 0x7a, 0x0f, 0xfd, 0x88, 0x92, 0x3c, 0xa6, 0xda, 0x46, 0xb5, 0x12, 0x58, 0xfd, 0xf3, 0xfe, 0xfe, 0xf8, 0xa9, 0x1e, 0x9e, 0x46, 0x32, 0x7d, 0xb9, 0x57, 0x1d, 0x82, 0x23, 0xbf, 0x2c, 0x1a, 0x89, 0xe7, 0xc9, 0x11, 0xa5, 0x47, 0x3a, 0x7e, 0x17, 0xca, 0x62, 0xdd, 0x6f, 0x3d, 0xe5, 0xa8, 0x6b, 0x95, 0xa6, 0x6b, 0xf1, 0xf5, 0xda, 0x27, 0xd1, 0x5d, 0x60, 0xba, 0xc8, 0x87, 0xda, 0x05, 0xef, 0xf7, 0xe6, 0xa9, 0x82, 0xd6, 0x0b, 0x6a, 0x54, 0xe6, 0xa3, 0x61, 0xef, 0x2a, 0x58, 0xba, 0x50, 0x32, 0x66, 0x5c, 0x9a, 0xc9, 0xbd, 0xbd, 0xcf, 0x8d, 0xe7, 0xa8, 0xfe, 0x6b, 0xce, 0xc9, 0xd1, 0xa0, 0x68, 0xf1, 0x5f, 0x9c, 0x8a, 0xdf, 0x87, 0x41, 0xfd, 0x90, 0x32, 0x21, 0xc7, 0xcc, 0xf3, 0xf3, 0x8a, 0x7d, 0xc5 }; } }
        //internal static byte[] PublicKey_ExponentBinary { get { return new byte[] { 0x01, 0x00, 0x01 }; } }


    }
}
