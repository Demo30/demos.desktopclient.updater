Cross-application updating tool
--------------------------------------------------

Designed to be able of udpating any of desktop application that can be updated merely by deleting/copying new content in its root folder.

Each version package is signed at its creation and verified when downloaded.

There are 3 main parts and one dependency (Demos.DemosHelpers).
- Demos.DesktopClient.Updater.Manager = the core library through which the developer can create new data and version-info package and the user can download, verify and install new version.
- Demos.DesktopClient.Updater.ManagerInterfacing = library of interface declarations to be used in the specific application. The particular application does not need to be dependent on any specific implementation, it only needs to know what interface the above mentioned manager implements. The application can then load the manager assembly at runtime and communicate with it.
- Demos.DesktopClient.Updater.GUI = GUI part for instructing the manager library


Usage:

- best practice seems to be having the three assemblies in a separate folder within the root aplication folder, e.g.: "../root/Updater/..."
- the updater does not update itself
- in case the interface got updated, its new version needs to be shipped with the new version of the application




