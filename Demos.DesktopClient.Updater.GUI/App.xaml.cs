﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Demos.Warden.Updater
{

    public partial class App : Application
    {
        public App()
        {
            this.DispatcherUnhandledException += ExceptionHandling;
        }

        private void ExceptionHandling(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
#if DEBUG
            MessageBox.Show(e.Exception.ToString());
#endif
#if !DEBUG
            MessageBox.Show(e.Exception.Message.ToString());
#endif
            e.Handled = true;
        }
    }
}
