﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Demos.DemosHelpers;

namespace Demos.DesktopClient.Updater
{
    /// <summary>
    /// Interaction logic for MetadataWindow.xaml
    /// </summary>
    public partial class MetadataWindow : Window
    {
        public string AppName { get { return this.txtbx_ApplicationName.Text; } }
        public string UpdateURL { get { return this.txtbx_UpdatePath.Text; } }
        public Versioning Version
        { 
            get 
            {
                Versioning v = new Versioning();
                v.LoadVersion(this.txtbx_Version.Text, ".");
                return v;
            }
        }
        public bool IsSuccess { get; private set; }

        private MetadataWindow()
        {
            InitializeComponent();
        }

        public MetadataWindow(string appName, string updateURL, Versioning version) : this()
        {
            this.txtbx_ApplicationName.Text = appName;
            this.txtbx_UpdatePath.Text = updateURL;
            this.txtbx_Version.Text = version == null ? "" : version.ToString();
        }

        private void btn_OK_Click(object sender, RoutedEventArgs args)
        {
            bool validState = !(String.IsNullOrEmpty(this.AppName) || String.IsNullOrEmpty(this.UpdateURL) || this.Version.Version == null || this.Version.Version.Length == 0);

            this.IsSuccess = validState;
            this.Close();
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs args)
        {
            this.IsSuccess = false;
            this.Close();
        }

    }
}
