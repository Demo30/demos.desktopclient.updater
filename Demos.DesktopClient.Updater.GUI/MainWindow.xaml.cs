﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Demos.DemosHelpers;

namespace Demos.DesktopClient.Updater
{
    public partial class MainWindow : Window
    {
        private UpdaterManager _manager;

        private Control[] _updateTabControlsAccordingToState = null;
        private Control[] _adminTabControlsAccordingToState = null;

        private MetadataWindow _windowMetadata = null;

        private string _pathToKeys = String.Empty;
        private string _pathToDataArchive = String.Empty;
        private string PathToUpdateDataPackage { get { return this.txtbx_pathToUpdateData.Text; } }
        private string _metadata_updateDataPackageAddress = String.Empty;
        private Versioning _metadata_version = null;

        public MainWindow()
        {
            InitializeComponent();

            this._manager = new UpdaterManager();

            this._manager.AvailableDataFileChanged += OnDataChanged;

            bool configSuccess = this._manager.LoadConfiguration(Environment.CurrentDirectory);

            if (configSuccess)
            {
                this.PrepareControls();
            }
            else
            {
                MessageBox.Show($"Expected configuration file \"{UpdaterConfiguration.CONFIG_FILENAME}\" is either missing or not in valid state. Shutting down updater.", "Critical error");
                Application.Current.Shutdown();
            }
        }

        private void PrepareControls()
        {
            this.lbl_version.Content = UpdaterGUICommons.Version.ToString();
            this.PrepareControls_UpdateTab();
            this.PrepareControls_AdminTab();

#if !AdminRelease
            this.tc_Admin.Visibility = Visibility.Hidden;
#endif
        }

        private void PrepareControls_UpdateTab()
        {
            this._updateTabControlsAccordingToState = new Control[]
            {
                this.btn_Update
            };

            this.txtbx_pathToUpdatedAppRoot.Text = this._manager.Config.RootDirectoryPath;

            this.UpdateAccordingToState_UpdateTab();
        }

        private void PrepareControls_AdminTab()
        {
            this._adminTabControlsAccordingToState = new Control[]
            {
                this.btn_CreateUpdateDataPackages
            };

            this.UpdateAccordingToState_AdminTab();
        }

        private void UpdateAccordingToState_UpdateTab()
        {
            this.ToggleControlState(this._updateTabControlsAccordingToState, this._manager.ValidDataPackageLoaded);
        }

        private void UpdateAccordingToState_AdminTab()
        {
            this.ToggleControlState(this._adminTabControlsAccordingToState, this.ReadyToBuildDataPackage());
        }

#region UPDATE tab

        private void btn_selectDataFile_Click(object sender, RoutedEventArgs args)
        {
            System.Windows.Forms.OpenFileDialog dia = new System.Windows.Forms.OpenFileDialog();
            System.Windows.Forms.DialogResult result = dia.ShowDialog();

            try
            {
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    this._manager.LoadUpdateData(dia.FileName, this._manager.Config);
                    this.txtbx_pathToUpdateData.Text = dia.FileName;
                }
            }
            catch
            {
                MessageBox.Show("Data se nepodařilo načíst. Aktualizace neprovedena. Pokud potíže přetrvávají, kontaktujte vývojáře.","Chyba");
                this.txtbx_pathToUpdateData.Text = String.Empty;
                return;
            }
            finally
            {
                this.UpdateAccordingToState_UpdateTab();
            }

        }

        private void btn_loadFromAddress_Click(object sender, RoutedEventArgs args)
        {
            if (File.Exists(this.PathToUpdateDataPackage))
            {
                this._manager.LoadUpdateData(this.PathToUpdateDataPackage, this._manager.Config);
            }
            else
            {
                this._manager.LoadUpdateDataURL(this.PathToUpdateDataPackage, this._manager.Config);
            }

            if (this._manager.ValidDataPackageLoaded == false)
            {
                MessageBox.Show($"Data z adresy: \"{this.PathToUpdateDataPackage}\" se nepodařilo načíst. Ujistěte se, že máte správnou adresu.");
            }
        }

        private void btn_getAddress_Click(object sender, RoutedEventArgs args)
        {
            if (String.IsNullOrEmpty(this._manager.Config.CheckVersionURL))
            {
                MessageBox.Show("Adresa pro ověření nové verze není známá. Nelze pokračovat.");
                return;
            }

            string updatePackageUrl = String.Empty;

            try
            {
                InfoData info = this._manager.RetrieveUpdateInfoPackage(this._manager.Config);
                updatePackageUrl = info.UpdateDataURL;
                this.txtbx_pathToUpdateData.Text = updatePackageUrl;

            }
            catch
            {
                if (String.IsNullOrEmpty(updatePackageUrl))
                {
                    MessageBox.Show($"Nepodařilo se získat URL adresu vedoucí k aktualizačnímu balíčku. Aplikace se na adresu dotazovala na cestě: \"{this._manager.Config.CheckVersionURL}\" (uvedena v konfiguračním souboru)", "Problém");
                }
            }
        }

        private void btn_Update_Click(object sender, RoutedEventArgs args)
        {
            string rootDirPathTextBox = this.txtbx_pathToUpdatedAppRoot.Text;
            if (String.IsNullOrEmpty(rootDirPathTextBox) == false && rootDirPathTextBox != this._manager.Config.RootDirectoryPath)
            {
                this._manager.Config.RootDirectoryPath = rootDirPathTextBox;
            }

            if (Directory.Exists(this._manager.Config.RootDirectoryPath) == false)
            {
                MessageBox.Show($"Uvedená složka \"{this._manager.Config.RootDirectoryPath}\" neexistuje.");
                return;
            }

            try
            {
                this._manager.PerformUpdate();
                MessageBox.Show("Aktualizace proběhla úspěšně!", "Úspěch");
            }
            catch
            {
                MessageBox.Show("Aktualizace aplikace se nezdařila. Pokud problémy přetrvávají, kontaktujte správce.", "Chyba");
            }


        }

        private void btn_selectUpdatedAppRoot_Click(object sender, RoutedEventArgs args)
        {
            System.Windows.Forms.FolderBrowserDialog dia = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dia.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.txtbx_pathToUpdatedAppRoot.Text = dia.SelectedPath;
            }
        }

#endregion

#region ADMIN tab

        private void btn_selectKeyFile_Click(object sender, RoutedEventArgs args)
        {
            System.Windows.Forms.OpenFileDialog dia = new System.Windows.Forms.OpenFileDialog();
            System.Windows.Forms.DialogResult result = dia.ShowDialog();

            try
            {
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    this.txtbx_pathToKeyFile.Text = dia.FileName;

                    UpdaterManager up = new UpdaterManager();

                    if (up.DoesFileContainValidPrivateKey(dia.FileName, this._manager.Config) == false)
                    {
                        throw new InvalidOperationException();
                    }
                    else
                    {
                        _pathToKeys = dia.FileName;
                    }
                }
            }
            catch
            {
                MessageBox.Show("Zvolený soubor není platným podpisovým klíčem.", "Neplatný úkon");
                this.txtbx_pathToKeyFile.Text = String.Empty;
                return;
            }
            finally
            {
                this.UpdateAccordingToState_AdminTab();
            }
        }

        private void btn_selectDataArchiveFile_Click(object sender, RoutedEventArgs args)
        {
            System.Windows.Forms.OpenFileDialog dia = new System.Windows.Forms.OpenFileDialog();
            System.Windows.Forms.DialogResult result = dia.ShowDialog();

            try
            {
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    if (this._manager.IsFileAZipArchiveFile(dia.FileName))
                    {
                        this.txtbx_pathToDataARchiveFile.Text = dia.FileName;
                        this._pathToDataArchive = dia.FileName;
                    }
                    else
                    {
                        throw new InvalidDataException();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Zvolený soubor není platným archivem ZIP.", "Neplatný úkon");
                this.txtbx_pathToDataARchiveFile.Text = String.Empty;
                return;
            }
            finally
            {
                this.UpdateAccordingToState_AdminTab();
            }
        }

        private void btn_editMetadata_Click(object sender, RoutedEventArgs args)
        {
            if (this._windowMetadata != null) { return; }

            this._windowMetadata = new MetadataWindow(this._manager.Config.ApplicationName, this._metadata_updateDataPackageAddress, this._metadata_version);
            this._windowMetadata.Closed += (obj, e) =>
            {
                if (this._windowMetadata.IsSuccess)
                {
                    this._metadata_updateDataPackageAddress = this._windowMetadata.UpdateURL;
                    this._metadata_version = this._windowMetadata.Version;
                }

                this._windowMetadata = null;
            };

            this._windowMetadata.Show();
        }

        private void btn_CreateUpdateDataPackages_Click(object sender, RoutedEventArgs args)
        {
            byte[] newVersionZippedData = null;

            string path = this._pathToDataArchive;

            if (File.Exists(path) == false)
            {
                throw new InvalidOperationException();
            }

            string destinationDir = this.GetDestinationDirectory();

            if (String.IsNullOrEmpty(destinationDir) == true)
            {
                return;
            }

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                newVersionZippedData = new byte[fs.Length];
                fs.Read(newVersionZippedData, 0, newVersionZippedData.Length);
            };

            InfoData info = new InfoData()
            {
                ApplicationName = this._manager.Config.ApplicationName,
                UpdateDataURL = this._metadata_updateDataPackageAddress,
                Version = this._metadata_version
            };
            string infoPckgDestination = String.Concat(destinationDir, "\\", "updateInfoPackage.zip");

            UpdaterManager updater = new UpdaterManager();
            byte[] updateInfo = updater.CreateUpdateInfoPackage(infoPckgDestination, this._pathToKeys, info);
            updater.CreateDataPackage(String.Concat(destinationDir,"\\", "updateDataPackage.zip"), newVersionZippedData, this._pathToKeys, updateInfo);

        }

        private void OnDataChanged(object sender, EventArgs args)
        {
            if (this._manager.ValidDataPackageLoaded == true)
            {
                this.lbl_dataFileStatus.Content = "Data pro provedení aktualizace jsou připravená, lze spustit aktualizaci.";
            }
            else
            {
                this.lbl_dataFileStatus.Content = "Nejsou k dispozici data pro aktualizaci.";
            }

            this.UpdateAccordingToState_UpdateTab();
        }

#endregion

#region Supportive methods

        private void ToggleControlState(Control[] controls, bool isEnabled)
        {
            for (int i = 0; i < controls.Length; i++)
            {
                controls[i].IsEnabled = isEnabled;
            }
        }

        private bool ReadyToBuildDataPackage()
        {
            if (File.Exists(this._pathToKeys) && File.Exists(this._pathToDataArchive))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string GetDestinationDirectory()
        {
            System.Windows.Forms.FolderBrowserDialog dia = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dia.ShowDialog();

            string directory = String.Empty;

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                directory = dia.SelectedPath;
            }

            return directory;
        }


#endregion
    }
}
