﻿using Demos.DemosHelpers;

namespace Demos.DesktopClient.Updater
{
    public interface IInfoData
    {
        bool IsValid { get; }
        string UpdateDataURL { get; }
        string ApplicationName { get; }
        Versioning Version { get; }
        string[] ChangesInVersion { get; }
    }
}
