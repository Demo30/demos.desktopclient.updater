﻿using Demos.DemosHelpers;

namespace Demos.DesktopClient.Updater
{
    public interface INewerVersionChecker
    {
        bool IsNewerVersionAvailable(Versioning version);
        IInfoData GetLatestVersionInfoData();
        bool LoadConfiguration(string pathToConfigurationFile);
    }
}
